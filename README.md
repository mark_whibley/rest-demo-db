# rest-demo-db

A sample database intended for use by other `rest-demo-xxx` projects.

This repository sets out to follow the excellent guidelines proposed by K. Scott Allen in his [Ode to Code blog](https://odetocode.com/), where he proposes [three rules for database work](https://odetocode.com/blogs/scott/archive/2008/01/30/three-rules-for-database-work.aspx):

**1. Never use a shared database for development work**

**2. Always have a single, authoritative source for your schema**

**3. Always version your database**

https://blog.codinghorror.com/get-your-database-under-version-control/